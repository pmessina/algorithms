#ifndef ARRAYS_H
#define ARRAYS_H

typedef struct SubarraySum {
    int left;
    int right;
    int sum; 
} subsum;

typedef struct Matrix {
    int m;
    int n;
    int* a;
} matrix;

/* Fill an array with integers */
void fill_array_int(int* a, int size);
void fill_array_int_search(int* a, int size, int containedInt);

/* Sort the array using a passed function (for example)
 * sort_array(insertionSort, argc, argv);
 */
void sort_array(void (*f)(int*, int), int argc, char* argv[]);

/*
ii * Multiply matrices together
 */
void multiply_matrix(matrix (*f)(matrix, matrix));

/* Find the max subbarray using a passed function (for example)
 * find_maxsub(maxsub, argc, argv);
 */
void find_maxsub(subsum (*f)(int*, int),int argc, char* argv[]);

/*
 * Search an array for a specified value
 */
void search_array(int (*f)(int*, int, int), int argc, char* argv[]);

/*  Take commandline array, and make it an array. */
void set_array(int argc, char* argv[]);

int is_sorted(int* a, int size, int lessToGreater);
void print_array(int* a, int size);

/* Set an mxn matrix. Uses malloc, the array will need to be freed */
matrix set_matrix();
void print_matrix(matrix A);

#endif /* ifndef ARRAYS_H */
