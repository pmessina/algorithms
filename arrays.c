#include "arrays.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

void fill_array_int(int* a, int size) {

    int i;

    srand(time(NULL));

    for (i=0; i < size; i++) {
        a[i] = rand();
    }

}

void fill_array_int_search(int *a, int size, int containedInt) {

    int i;

    fill_array_int(a, size);
    /* insert contained int somewhere in the array. */
    i = rand() % size;
    a[i] = containedInt;
}

int is_sorted(int *a, int size, int lessToGreater) {
    int i;
    if (lessToGreater) {
        for (i=0; i<size-1; i++) {
            if (a[i] > a[i+1])
                return 0;
        }
    }
    return 1;
}

void find_maxsub(subsum (*f)(int*, int), int argc, char* argv[]) {

    int i;
    int size;

    clock_t t;
    double time_taken;

    subsum solution;

    int* array;
    size = argc - 1;
    array = (int*)malloc(size*sizeof(int));

    for (i=0; i<argc-1; i++)
        array[i] = atoi(argv[i+1]);

    t = clock();

    solution = (*f)(array, size);

    t = clock() - t;
    time_taken = ((double)t)/CLOCKS_PER_SEC;



    printf("\n\nSum of %d in %fs\n", solution.sum, time_taken);
    printf("Left Index: %d\n", solution.left);
    printf("Right Index: %d\n", solution.right);
    printf("Subarray:");
    for (i=solution.left; i<=solution.right; i++)
        printf("%d ", array[i]);
    printf("\n");

    free(array);

}

void sort_array(void (*f)(int*, int), int argc, char* argv[]) {

    int i;
    int size;

    /* Used to measure time for function to sort. */
    clock_t t;
    double time_taken;

    int* array;

    size = argc - 1;

    array = (int*)malloc(size*sizeof(int));


    /* Convert string to integers */
    for (i=0; i<argc-1; i++)
        array[i] = atoi(argv[i+1]);

    printf("Presorted: ");
    print_array(array, size);

    t = clock();

    /* run the sorting function */
    (*f)(array, size);

    t = clock() - t;
    time_taken = ((double)t)/CLOCKS_PER_SEC;

    printf("\n\nSorted %s in %fs: ", argv[0], time_taken);
    print_array(array, size);

    free(array);

}

void multiply_matrix(matrix (*f)(matrix, matrix)) {

    clock_t t;
    double time_taken;

    matrix A;
    matrix B;
    matrix C;

    printf("Set matrix A:\n");
    A = set_matrix();

    printf("Set matrix B:\n");
    B = set_matrix();

    t = clock();

    /* run the sorting function */
    C = (*f)(A, B);

    t = clock() - t;
    time_taken = ((double)t)/CLOCKS_PER_SEC;

    printf("\n\nMultiplied matrix A times B in %fs: ", time_taken);
    print_matrix(C);

    free(A.a);
    free(B.a);
    free(C.a);

}

void search_array(int (*f)(int*, int, int), int argc, char* argv[]){

    int i;
    int size;

    /* Used to measure time for function to sort. */
    clock_t t;
    double time_taken;

    int* array;

    int value;

    int location;

    if (strcmp(argv[1], "-v") == 0)
        value = atoi(argv[2]);
    else {
        fprintf(stderr, "Expected arguement -v to search for.\n");
        exit(1);
    }

    size = argc - 3;

    array = (int*)malloc(size*sizeof(int));

    /* Convert string to integers */
    for (i=0; i<argc-3; i++)
        array[i] = atoi(argv[i+3]);

    t = clock();

    /* run the search function */
    location = (*f)(array, size, value);

    t = clock() - t;
    time_taken = ((double)t)/CLOCKS_PER_SEC;

    printf("Searched %s in %fs: \n", argv[0], time_taken);

    if (location == -1) 
        printf("The value %d is not contained in the given array.", value);
    else
        printf("Value: %d\nLocation: %d\n", value, location);

    free(array);

}

void print_array(int* a, int size) {
    int i;
    for (i=0; i<size; i++)
        printf("%d ", a[i]);
}

matrix set_matrix() {

    matrix A;
    int m;
    int n;

    int i;
    int j;


    printf("For an mxn matrix, enter a natural number for m: ");
    scanf("%d", &m);
    printf("For an mxn matrix, enter a natural number for n: ");
    scanf("%d", &n);

    A.a = (int*)malloc(n * m * sizeof(int));

    for (i=0; i < m; i++)
        for (j=0; j < n; j++)
            scanf("%d", &*(A.a + i*n + j));

    A.m = m;
    A.n = n;

    return A;

}

void print_matrix(matrix A) {

    int i;
    int j;

    for (i = 0; i <  A.m; i++) {
      for (j = 0; j < A.n; j++) {
         printf("%d ", *(A.a + i*A.n + j));
         if (j == A.n - 1)
             printf("\n");
      }
    }

}

