#include "arrays.h"
#include "binarysearch.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

    search_array(binary_search, argc, argv);

    return 0;
}

int binary_search(int* a, int size, int value) {

    int index = _binary_search(a, value, 0, size-1);

    return index;

}

int _binary_search (int* a, int value, int l, int r) {

    if (l <= r) {

        int m = l + (r - l) / 2;

        if (value == a[m])
            return m;

        if (value < a[m]) 
            return _binary_search(a, value, l, m - 1);

        return _binary_search(a, value, m + 1, r);

    }

    return -1;

}
