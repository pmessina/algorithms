#include "arrays.h"
#include "insertionsort.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

    sort_array(insertion_sort, argc, argv);

    return 0;
}

void insertion_sort(int* a, int size) {
    int j;
    for (j = 1; j < size; j++) {
        int key = a[j];
        int i = j - 1;
        while (i > -1 && a[i] > key) {
            a[i + 1] = a[i];
            i = i - 1;
        }
        a[i+1] = key;
    }
}
