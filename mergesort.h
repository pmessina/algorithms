#ifndef MERGESORT_H
#define MERGESORT_H value

/* inital call for mergesort, used to keep consistent with other sorting
 * alogrithms */
void merge_sort(int* a, int size);

/* actual mergesorting */
void _merge_sort(int* a, int l, int r);

/* a is the array, l,m,r are indices such that l <= m < r */
void merge(int* a, int l, int m, int r);

#endif /* ifndef MERGESORT_H */
