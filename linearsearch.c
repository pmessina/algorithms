#include "arrays.h"
#include "linearsearch.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

    search_array(linear_search, argc, argv);

    return 0;
}

int linear_search(int* a, int size, int value) {

    int i;

    for (i=0; i < size; i++) 
        if (value == a[i])
            return i;

    return -1;
}
