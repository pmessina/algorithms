#include "stack.h"

#define STACK_SIZE 100

int main(int argc, char *argv[])
{


    int a[STACK_SIZE];
    struct IntStack s;

    int i;

    s.top = -1;
    s.stack = a;
    s.max = STACK_SIZE;

    int_stack_push(&s, 10);
    int_stack_push(&s, 12);
    int_stack_push(&s, 13);
    int_stack_push(&s, 14);

    while (!int_stack_is_empty(&s))
        printf("%d\n", int_stack_pop(&s));

    return 0;
}
