#include "maxsub.h"

int main(int argc, char *argv[]) {

    find_maxsub(maxsub, argc, argv);

    return 0;
}

/* Recursive */
subsum maxsub(int* a, int size) {
    return _maxsub(a, 0, size-1);
}

/* Brute force */
/* subsum maxsub(int* a, int size) { */
/*     subsum solution; */ 
/*     int sum; */
/*     int i; */
/*     int j; */

/*     solution.sum = INT_MIN; */
/*     solution.left = INT_MIN; */
/*     solution.right = INT_MAX; */

/*     for (i=0; i < size; i++) { */

/*         sum = a[i]; */

/*         if (sum > solution.sum) { */
/*             solution.left = i; */
/*             solution.right = i; */
/*             solution.sum = sum; */
/*         } */


/*         for (j=i+1; j < size; j++) { */

/*             sum += a[j]; */

/*             if (sum > solution.sum) { */
/*                 solution.left = i; */
/*                 solution.right = j; */
/*                 solution.sum = sum; */
/*             } */
/*         } */

    /* } */

    /* return solution; */
/* } */

subsum _maxsub(int* a, int l, int h) {

    subsum base_max;
    subsum left_max;
    subsum mid_max;
    subsum right_max;

    int m;

    /* Base case is a single element */
    if (l == h) {

        base_max.left = l;
        base_max.right = h;
        base_max.sum = a[l];

        return base_max;
    }

    m = l + (h - l) / 2;

    left_max =  _maxsub(a, l, m);
    right_max =  _maxsub(a, l, m);
    mid_max =  maxsub_crossing(a, l, m, h);

    if (left_max.sum >= right_max.sum
            && left_max.sum >= mid_max.sum)
        return left_max;

    if (right_max.sum >= left_max.sum
            && right_max.sum >= mid_max.sum)
        return right_max;

    return mid_max;

}

subsum maxsub_crossing(int* a, int l, int m, int h) {

    subsum subarray;

    int left_sum;
    int right_sum;
    int sum;

    int i;

    left_sum = INT_MIN;
    right_sum = INT_MIN;
    sum = 0;

    subarray.left = m;
    subarray.right = m;
    subarray.sum = a[m];

    for (i=m; i>=l; i--) {
        sum = sum + a[i];
        if (sum > left_sum) {
            left_sum = sum;
            subarray.left = i;
        }
    }

    sum = 0;
    for (i=m+1; i<=h; i++) {
        sum = sum + a[i];
        if (sum > right_sum) {
            right_sum = sum;
            subarray.right = i;
        }
    }

    subarray.sum = subarray.left + subarray.right;

    return subarray;

} 
