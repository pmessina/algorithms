/*
 * Linear Search
 *
 * Search for the first instance of a specified integer, and return
 * the index for said value or -1 if it is not found.
 *
 */

#ifndef LINEARSEARCH_H
#define LINEARSEARCH_H value

int linear_search(int* array, int size, int value);

#endif /* ifndef LINEARSEARCH_H */
