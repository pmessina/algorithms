# Algorithms

The following is my work learning algorithms, data structures, and the analysis
thereof. This repo will contain work from a variety of sources. In particular
the book *Introduction to Algorithms* by Thomas H. Cormen, Charles E.
Leiserson, Ronald L. Rivest, and Clifford Stein and *The Art of Computer
Programming* by Donald E. Knuth. The algorithms themselves will be in the top
directory, and any problems I may type out will be in the respective
directories `clsr/` and `acp/`.

# Notes

Any usage of `log` will be in base 2.
