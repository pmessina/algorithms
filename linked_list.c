#include "linked_list.h"


void print_int_linked_list(struct IntLinkedList *l) 
{
    struct IntNode *x;
    x = l->head;

    while (x != NULL) {
        printf("%d ", x->integer);
        x = x->next;
    }
    printf("\n");
}


void int_linked_insert(struct IntLinkedList *l, struct IntNode *n) 
{

    n->next = l->head;

    if (l->head != NULL)
        l->head->prev = n;
    l->head = n;
    n->prev = NULL;

}


struct IntNode *int_linked_search(struct IntLinkedList *l, int k) 
{

    struct IntNode *x;

    x  = l->head;

    while (x->next != NULL && x->integer != k)
        x = x->next;

    return x;
}


void int_delete_linked_node(struct IntLinkedList *l, struct IntNode *n) 
{
    if (n->prev != NULL)
        n->prev->next = n->next;
    else
        l->head = n->next;

    if (n->next != NULL)
        n->next->prev = n->prev;
}


void print_char_linked_list(struct CharLinkedList *l)
{
    struct CharNode *x;
    x = l->head;

    while (x != NULL) {
        printf("%s\n", x->string);
        x = x->next;
    }
    printf("\n");
}


void char_linked_insert(struct CharLinkedList *l, struct CharNode *n)
{

    n->next = l->head;

    if (l->head != NULL)
        l->head->prev = n;
    l->head = n;
    n->prev = NULL;

}


struct CharNode *char_linked_search(struct CharLinkedList *l, char *s)
{

    struct CharNode *x;

    x  = l->head;

    while (x->next != NULL && !strcmp(x->string, s))
        x = x->next;

    return x;
}


void char_delete_linked_node(struct CharLinkedList *l, struct CharNode *n)
{
    if (n->prev != NULL)
        n->prev->next = n->next;
    else
        l->head = n->next;

    if (n->next != NULL)
        n->next->prev = n->prev;
}
