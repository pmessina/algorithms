#ifndef SQUARE_MULT_H
#define SQUARE_MULT_H value

#include "arrays.h"

/* Multiply square matrices together */
matrix multi(matrix A);

#endif /* ifndef SQUARE_MULT_H */
