CC = gcc
CFLAGS = -pedantic -Wall -Wextra -ansi
FILES = arrays.c

# Sorting

insertionsort:
	$(CC) $(CFLAGS) $(FILES) insertionsort.c -o bin/insertionsort

bubblesort:
	$(CC) $(CFLAGS) $(FILES) bubblesort.c -o bin/bubblesort

mergesort:
	$(CC) $(CFLAGS) $(FILES) mergesort.c -o bin/mergesort

# Searching

binarysearch:
	$(CC) $(CFLAGS) $(FILES) binarysearch.c -o bin/binarysearch

linearsearch:
	$(CC) $(CFLAGS) $(FILES) linearsearch.c -o bin/linearsearch

maxsub:
	$(CC) $(CFLAGS) $(FILES) maxsub.c -o bin/maxsub

square-matrix:
	$(CC) $(CFLAGS) $(FILES) square-matrix.c -o bin/square-matrix

linked-list:
	$(CC) $(CFLAGS) $(FILES) linked_list_main.c linked_list.c -o bin/linked-list

stack:
	$(CC) $(CFLAGS) $(FILES) stack_main.c stack.c -o bin/stack
