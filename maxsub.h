/*
 * Find Max Subbarray
 *
 * Given an array of integers, find the max subbarray.
 *
 */


#include "arrays.h"
#include <limits.h>

#ifndef MAXSUB_H
#define MAXSUB_H value

/* Initial call to find the maximum subbarray */
subsum maxsub(int* array, int size);

/* Actual function that does the work. */
subsum _maxsub(int* array, int l, int r);

/* Find the max subbarray across a midpoint */
subsum maxsub_crossing(int* array, int l, int m, int r);

#endif /* ifndef MAXSUB_H */
