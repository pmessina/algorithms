#include "arrays.h"
#include "mergesort.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {

    sort_array(merge_sort, argc, argv);

    return 0;
}

void merge_sort(int* a, int size) {
    _merge_sort(a, 0, size-1);
}

void _merge_sort(int* a, int l, int r) {
    if (l < r) {
        int m = l + (r - l) / 2;
        _merge_sort(a, l, m);
        _merge_sort(a, m + 1, r);
        merge(a, l, m, r);
    }
}

void merge(int* a, int l, int m, int r) {

    /* indexes */
    int i, j, k;

    /* sizes */
    int n1 = m - l + 1;
    int n2 = r - m;

    /* new arrays of appropriate size */
    int* left = (int*)malloc(n1 * sizeof(int));
    int* right = (int*)malloc(n2 * sizeof(int));

    for (i=0; i<n1; i++)
        left[i] = a[l + i];
    for (j=0; j<n2; j++)
        right[j] = a[m + 1 + j];

    i = 0;
    j = 0;
    k = l;

    /* actual sorting */
    while (i < n1 && j < n2) {
        if (left[i] <= right[j])
            a[k++] = left[i++];
        else
            a[k++] = right[j++];
    }

    /* copy over whatever is left */
    while (i < n1)
        a[k++] = left[i++];
    while (j < n2)
        a[k++] = right[j++];

    free(left);
    free(right);

}
