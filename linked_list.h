#ifndef LINKED_LIST_H
#define LINKED_LIST_H value

/*
 * The following implementation of the linked list is doubly linked.
 */


#include <stdio.h>
#include <string.h>

struct IntLinkedList{
    struct IntNode *head;
    struct IntNode *tail;
};


struct IntNode {
    int integer;
    struct IntNode *next;
    struct IntNode *prev;
};


struct CharLinkedList{
    struct CharNode *head;
    struct CharNode *tail;
};


struct CharNode {
    char *string;
    struct CharNode *next;
    struct CharNode *prev;
};

/*
 * Print out linked list from head to tail
 */
void print_int_linked_list(struct IntLinkedList *l);

/*
 * Insert a value into the front of the linked list (assuming
 * nonempty)
 */
void int_linked_insert(struct IntLinkedList *l, struct IntNode *n);

/*
 * Searc the first element with the key k in a linked list l 
 */
struct IntNode *int_linked_search(struct IntLinkedList *l, int k);

/*
 * Delete given node
 */
void int_delete_linked_node(struct IntLinkedList *l, struct IntNode *n);


/*
 * Print out linked list from head to tail
 */
void print_char_linked_list(struct CharLinkedList *l);

/*
 * Insert a value into the front of the linked list (assuming
 * nonempty)
 */
void char_linked_insert(struct CharLinkedList *l, struct CharNode *n);

/*
 * Searc the first element with the key k in a linked list l 
 */
struct CharNode *char_linked_search(struct CharLinkedList *l, char *s);

/*
 * Delete given node
 */
void char_delete_linked_node(struct CharLinkedList *l, struct CharNode *n);

#endif /* ifndef LINKED_LIST_H */
