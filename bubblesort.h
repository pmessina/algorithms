/*
 * Bubble Sort
 *
 * Sort an integer array using bubble sort.
 *
 */

#ifndef BUBBLESORT_H
#define BUBBLESORT_H value

void bubble_sort(int* array, int size);

#endif /* ifndef BUBBLESORT_H */
