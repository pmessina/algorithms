#include "linked_list.h"
#include <stdio.h>


int main(int argc, char *argv[])
{

    struct IntLinkedList l;
    struct IntNode n;
    struct IntNode m;
    struct IntNode *o;

    struct CharLinkedList l2;
    struct CharNode s;
    struct CharNode t;
    struct CharNode *u;


    n.integer = 1;
    m.integer = 2;

    s.string = "Hello, Everyone!";
    t.string = "Goodbye, Everyone!";

    l.head = NULL;
    l.tail = NULL;

    l2.head = NULL;
    l2.tail = NULL;

    print_int_linked_list(&l);
    printf("---\n");
    int_linked_insert(&l, &n);
    int_linked_insert(&l, &m);
    print_int_linked_list(&l);

    o = int_linked_search(&l, 2);
    int_delete_linked_node(&l, o);
    print_int_linked_list(&l);

    printf("\n---\n");
    char_linked_insert(&l2, &s);
    char_linked_insert(&l2, &t);
    print_char_linked_list(&l2);

    u = char_linked_search(&l2, "Hello, World!");
    char_delete_linked_node(&l2, u);
    print_char_linked_list(&l2);

    return 0;
}
