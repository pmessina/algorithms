#include "stack.h"


int int_stack_is_empty(struct IntStack *s) {
    if (s->top  < 0)
        return 1;
    return 0;
}



int int_stack_push(struct IntStack *s, int n) {

    if (s->top + 1 >= s->max) {
        fprintf(stderr, "stack overflow: attempting to push %d\n", n);
        exit(EXIT_FAILURE);
    }

    s->top++;

    s->stack[s->top] = n;

    return 0;
}


int int_stack_pop(struct IntStack *s) {

    if (int_stack_is_empty(s)) {
        fprintf(stderr, "stack underflow: attempting to pop");
        exit(EXIT_FAILURE);
    }

    return s->stack[s->top--];

}
