/*
 * Binary Search
 *
 * Search for an instance of a specified integer, and return the index
 * for said value or -1 if it is not found. Assumes that the array is
 * already sorted.
 *
 */

#ifndef BINARYSEARCH_H
#define BINARYSEARCH_H value

int binary_search(int* array, int size, int value);
int _binary_search(int* array, int value, int l, int r);

#endif /* ifndef BINARYSEARCH_H */
