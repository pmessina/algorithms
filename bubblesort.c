#include "arrays.h"
#include "bubblesort.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

    sort_array(bubble_sort, argc, argv);

    return 0;
}

void bubble_sort(int* a, int size) {
    int i, j;
    for (i=0; i < size-1; i++) {
        for (j=size-1; j>i ; j--) {
            if (a[j] < a[j-1]) {
                int temp = a[j];
                a[j] = a[j-1];
                a[j-1] = temp;
            }
        }
    }
}
