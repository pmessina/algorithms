#ifndef STACK_H
#define STACK_H value

#include <stdio.h>
#include <stdlib.h>

struct IntStack {
    int top;
    int *stack;
    int max;
};


/*
 * Check if stack is empty
 */
int int_stack_is_empty(struct IntStack *s);

/*
 * Add element to top
 */
int int_stack_push(struct IntStack *s, int n);

/*
 * Pop element from top
 */
int int_stack_pop(struct IntStack *s);

#endif /* ifndef STACK_H */
