\documentclass[11pt]{amsart}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{mathrsfs}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{color}
\usepackage{listings}

\definecolor{gray}{rgb}{0.5,0.5,0.5}
\lstset{
  basicstyle=\ttfamily,
  numbers=left,
  numberstyle=\tiny\color{gray},
  mathescape
}

\newtheorem{problem}{Problem}[section]
\newtheorem*{problem*}{Problem}
\newtheorem{theorem}{Theorem}[section]
\newtheorem*{theorem*}{Theorem}
\newtheorem*{claim*}{Claim}

\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Zp}{\mathbb{Z}^{+}}
\newcommand{\N}{\mathbb{N}}

\title[CLRS 2.3]{CLRS Chapter 2}
\author{Patrick Messina}


\begin{document}

\maketitle 
\newpage 


% Problem 2.3-1
\begin{problem*}

  Illustrate the operation of merge sort on the array $A = \{3, 41, 52, 26, 38,
  57, 9, 49 \}$.

\end{problem*}

\begin{center}
  \includegraphics[scale=0.5]{3-1.jpg}
\end{center}


% Problem 2.3-2
\newpage
\begin{problem*}

  Rewrite the merge procedure so that it does not use sentinels, instead
  stopping once either array $L$ or $R$ has had all its elements copied back to
  $A$ and then copying the remainder of the other array back into $A$.
  
\end{problem*}

Implemented in source code for mergesort.

% Problem 2.3-3
\newpage
\begin{claim*}

  When $n$ is an exact power of $2$, the solution of the reccurence

  \[
    T(n) = 
    \begin{cases}
      2 & \text{if } n = 2 \text{,}\\
      2 T \left(\frac{n}{2} \right) + n & \text{if } n = 2^k \text{, for } k > 1
    \end{cases}
  \]

  \noindent is $T(n) = n \log n$.

\end{claim*}

\begin{proof}

  We proceed with mathematical induction. Let $n = 4$. Then

  \begin{align*}
    T(4) &= 2 T\left(\frac{4}{2} \right) + 4) \\
         &= 2(2) + 4 \\
         &= 8
  \end{align*}

  \noindent and 

  \[
    T(4) = 4 \log(4) = 8.
  \]

  Assume, for $n = 2^k$, the hypothesis holds. Let $n = 2^{k+1}$. Then we have

  \begin{align*}
    T(n) &= 2T\left(\frac{2^{k+1}}{2} \right) + 2^{k+1} \\
         &= 2T(2^{k}) + 2^{k+1} \\
         &= 2(2^k)k + 2^{k+1} \\
         &= 2^{k+1} k + 2^{k+1} \\
         &= 2^{k+1} (k + 1)\\
         &= n \log n.
  \end{align*}
  
\end{proof}

% Problem 2.3-4
\newpage
\begin{problem*}

  We can express insertion sort as a recursive procedure as follows. In order
  to sort $A[1 \dots n]$, we recursively sort $A[1 \dots n-1]$ and then insert
  $A[n]$ into the sorted array $A[1 \dots n-1]$. Write a recurrence for the
  worst-case running time of this recursive version of insertion sort.
  
\end{problem*}

\[
  T(n) =
  \begin{cases}
    \theta(1) & \text{if } n \le 1 \text{,}\\
    T (n - 1) + \theta(n) & \text{if } n > 1
  \end{cases}.
\]

% Problem 2.3-5
\newpage
\begin{problem*}

  Referring back to the searching problem (exercise 2.1-3), observe that if the
  sequence $A$ is sorted, we can check the midpoint of the sequence against
  $\upsilon$ and eliminate half of the sequence from further consideration. The
  \textbf{binary search} algorithm repeats this procedure, halving the size of
  the remaining portion of the sequence each time. Write pseudocode, either
  iterative or recursive, for binary search. Argue that the worst-case running
  time of binary search is $\Theta (\log n )$.

\end{problem*}

There is an implmentation in the top directory.

\begin{lstlisting}
// A is a sorted intger array
// $\upsilon \in \Z$ is the value being searched for
// l is the left index for the subarray of Array
// r is the right index for the subarray of Array
int binarySearch (int A, int $\upsilon$, int l, int r)
  if l <= r 
    // find the center of the array
    int m = l + (r - l) / 2
    // Stop if $\upsilon$ is at index m
    if $\upsilon$ == A[m]
        return m
    // Only look at the left half
    if $\upsilon$ < A[m] 
        return binarySearch(A, $\upsilon$, l, m - 1)
    // Otherwise look at the right half
    return binarySearch(A, $\upsilon$, m + 1, r)

  // $\upsilon$ is not found
  return -1
  
\end{lstlisting}

Let $n$ be the order of the sorted integer array $A$. Then the recurrence is 

\[
  T(n) =
  \begin{cases}
    \Theta (1) & \text{if } n = 1 \text{,}\\
    T (\frac{n}{2}) + \Theta(1) & \text{if } n > 1
  \end{cases}.
\]

Then at worst-case we have $\Theta (\log n)$.

% Problem 2.3-6
\newpage
\begin{problem*}

  Observe that the while loop of lines 7-9 of the insertion-sort
  procedure in Section 2.1 uses a linear search to scan (backward)
  through the sorted subarray $A[1 \dots j -1]$. Can we use a
  binary search instead to improve the overall worst-case running
  time of insertion sort to $\Theta(n \log n)$?
  
\end{problem*}

For convience, the following is pseudocode for insertion-sort:

\begin{lstlisting}
// A is an integer array
function insertionSort(int A)
  for j = 2 to A.length
     key = A[j]
     // Insert A[j] into the sorted sequence A[1 \dots j-1]
     i = j - 1
     while i > 0 and A[i] > key
       A[i+1] = A[i]
       i = i - 1
     A[i+1] = key
\end{lstlisting}

\noindent We now produce pseudocode for insertion-sort with a binary search
instead of linear search. 

\begin{lslisting}
// A is a sorted intger array
// $\upsilon \in \Z$ is the value being searched for
// l is the left index for the subarray of Array
// r is the right index for the subarray of Array
somethingfunction binarySearch(int A, int $\upsilon$, int l, int r)
  if l <= r
  
\end{lslisting}

\begin{lslisting}
// A is an integer array
function insertionSort(int A)
  for j = 2 to A.length
     key = A[j]
     // Insert A[j] into the sorted sequence A[1 \dots j-1]
     i = j - 1
     while i > 0 and A[i] > key
       A[i+1] = A[i]
       i = i - 1
     A[i+1] = key
\end{lslisting}

\end{document}
