/*
 * Insertion Sort
 *
 * Sort an integer array using insertion sort.
 *
 */

#ifndef INSERTIONSORT_H
#define INSERTIONSORT_H value

void insertion_sort(int* array, int size);

#endif /* ifndef INSERTIONSORT_H */
